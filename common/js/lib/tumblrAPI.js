(function(window, document) {
    "use strict";

	
    var domain = "0ness.tumblr.com",
        api_key = "3vtj0YCZQu620gzG4fr5r0CTRxYWeyjRA338IxuKhFNtVHcIeN",
        api_url = "http://api.tumblr.com/v2/blog/" + domain + "/posts?api_key=" + api_key;

	
    //tumblr読み込み後の処理
    var tumblrLoaded = function(evt) {
        var _posts 		= evt.response.posts,
            _tumblrBlc 	= document.getElementById('tumblr-container'),
            _domStr 	= "";

        for (var i = 0; i < 8; i = (i + 1) | 0) {
            var _post = _posts[i];
            if (_post.reblog.tree_html !== "") continue;

            switch (_post.type) {
                case "photo":
                    var _size 	= _post.photos[0].alt_sizes[3],
                        _width 	= _size.width,
                        _height = _size.height,
                        _ratio 	= _width / _height,
                        _cls 	= (_ratio > 1.4) ? "" : " is-near-square";

                    _domStr += "<div class='tumblr-item'>";
					_domStr += "<a href='" + _post.short_url + "' class='tumblr-item__img" + _cls + "' target='_blank'><img src='" + _size.url + "' alt=''></a>";
                    _domStr += "<span class='tumblr-item__title'>" + _post.caption + "</span>";
                    _domStr += "</div>";
                    break;

                case "video":
                    var _width 	= _post.thumbnail_width,
                        _height = _post.thumbnail_height,
                        _ratio 	= _width / _height,
                        _cls 	= (_ratio > 1.4) ? "" : " is-near-square";

                    _domStr += "<div class='tumblr-item'>";
                    _domStr += "<a class='tumblr-item__img" + _cls + "'><img src='" + _post.thumbnail_url + "' alt=''></a>";
                    _domStr += "<span class='tumblr-item__title'>" + _post.caption + "</span>";
                    _domStr += "</div>";
                    break;

                case "text":
                    break;
                case "quote":
                    break;
                case "link":
                    break;
                case "chat":
                    break;
                case "audio":
                    break;
            }
        }
        _tumblrBlc.innerHTML += _domStr;
    };


    //AJAX処理
    var init = function() {
        $(function() {
            $.ajax({
                url: api_url,
                dataType: "jsonp"
            }).done(tumblrLoaded);
        });

    }();

})(window, document);