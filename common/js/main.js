(function(window, document) {
    "use strict";




    /* constructor
	--------------------------------------------------------------------*/
    var Index = function() {
        this.init();
    },
        Member = Index.prototype;




    /* property
	--------------------------------------------------------------------*/
	var canvasFrame 	= document.getElementById('canvas-iframe'),
		canvasStyle		= canvasFrame.style,
		$canvasFrame	= $(canvasFrame),
		wrap 			= document.getElementById("wrapper"),
		sketchLink 		= document.getElementById("sketch").querySelectorAll("a.sketch-item__link"),
		canvasTitle		= document.getElementById('canvas-work'),
		columnLight  	= wrap.querySelectorAll('span.column__hd__light'),

        rnd_01 		= new LetterFader({id: "headName",duration:800,delay:40}),
		rnd_01_02 	= new LetterFader({id: "headName_02",duration:800,delay:40}),
		rnd_01_03 	= new LetterFader({id: "headName_03",duration:800,delay:40}),
		rnd_03 		= new LetterFader({id: "txtCopy_01",duration:1000,delay:20}),
		rnd_03_02 	= new LetterFader({id: "txtCopy_01_02",duration:1000,delay:20}),
		rnd_04 		= new LetterFader({id: "txtCopy_02",duration:600,delay:5}),
		rnd_04_02 	= new LetterFader({id: "txtCopy_02_02",duration:600,delay:5}),
		
		onBlackOut 		= false,
		timerBlackOut 	= null,
		pageHeight		= 0,
		frameHeight		= 0,
		canvasHeight	= 0;


    /* method
	--------------------------------------------------------------------*/
    Member.init = function() {
        this.initCanvasEvent();
		this.introAnim();
		
		pageHeight = wrap.offsetHeight;
		frameHeight = window.innerHeight;
		canvasHeight = canvasFrame.offsetHeight;
		
		document.getElementById('link').querySelector('.gmail a').setAttribute("href","mailto:" + "mizuiro0305" + "@gmail.com");
		window.addEventListener('scroll', this.scrollCanvas.bind(this));
    };
	
	/**
	 * イントロアニメーション
	 */
	Member.introAnim = function(){
		var _self = this;
		setTimeout(function() {
			document.getElementById("vi").classList.add("is-displayed");
			document.getElementById('mask').classList.add("is-hide");
			setTimeout(function(){_self.fadeInText();}, 800);
		}, 100);
	};

    /**
     * canvasコンテンツ切り替え
     */
    Member.initCanvasEvent = function() {
        var _self = this,
			_len  = sketchLink.length;
		
		for (var i=0; i<_len; i=(i+1)|0) {
			(function(i) {
				var _link = sketchLink[i];
				_link.addEventListener('click', function(e) {
					e.preventDefault();
					_self.changeCanvas(i);
				});
			})(i);
		};
		
		var _rnd = Math.random()*_len | 0;
		this.showCanvas(sketchLink[_rnd],0);
	};

    /**
     * canvasフェード処理
     * @param {string} _url ページURL
     */
    Member.changeCanvas = function(_num) {
		var _link 	= sketchLink[_num],
			_self   = this;

		canvasTitle.innerHTML = "<span>"+_link.querySelector('span.sketch-item__number').textContent + "</span>：" + _link.querySelector('span.sketch-item__name').textContent;
        $canvasFrame.fadeTo(500, 0, "linear", function() {
			_self.showCanvas(_link,500);
		});
    };
	
	/**
	 * [[Description]]
	 * @param {[[Type]]} _link [[Description]]
	 */
	Member.showCanvas = function(_link,_delay){
		var _url 	= _link.getAttribute("href");
		$canvasFrame.attr("src", _url).delay(_delay).fadeTo(800, 1, "easeOutQuint");
		document.documentElement.style.setProperty('--light-color', _link.getAttribute("data-color") );
	};

    /**
     * 暗転処理　初期化
     */
    Member.initBlackOut = function() {
		var _self = this;
		wrap.addEventListener("mousemove", this.resetScreen.bind(this), false);
		wrap.addEventListener("scroll", this.resetScreen.bind(this), false);
		timerBlackOut = setTimeout(this.blackOutScreen.bind(this), 14000);
    };
	
	/**
	 * 暗転処理 実行
	 */
	Member.blackOutScreen = function(){
		wrap.classList.add("fadeOut");
		onBlackOut= true;
	};
	
	/**
	 * 暗転処理　リセット
	 */
	Member.resetScreen = function(){
		var _self = this;
		clearTimeout(timerBlackOut);
		timerBlackOut = setTimeout(this.blackOutScreen.bind(this), 12000);
		
		if(onBlackOut === false) return false;
		onBlackOut = false;
		wrap.classList.remove("fadeOut");
	};

    /**
     * テキスト表示処理
     */
    Member.fadeInText = function() {
		var _self = this;

        rnd_01.back();
        rnd_01_02.back();
        rnd_01_03.back();
		
        setTimeout(function() {
            rnd_03.random();
            rnd_03_02.random();
            rnd_04.random();
            rnd_04_02.random();
            setTimeout(function() {
                document.getElementById("profile").style.opacity = 1;
                document.getElementById("about").style.opacity = 1;
                document.getElementById("sketch").style.opacity = 1;
                document.getElementById("works").style.opacity = 1;
				_self.initBlackOut();
            }, 900);
        }, 900);
    };

	/**
	 * canvasエリアをスクロールする
	 */
	Member.scrollCanvas = function(){
		var _scrMax = canvasHeight - frameHeight,
			_per 	= window.scrollY / pageHeight,
			_top 	= -(_scrMax * _per) | 0;
		canvasStyle.transform = "translate3d(0,"+ _top +"px,0)";
	};


    window.Index = Index;
}(window, document));




window.addEventListener('load', function(e) {
	var index = new Index();
});